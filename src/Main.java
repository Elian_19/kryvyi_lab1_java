import java.util.Arrays;

public class Main {
    public static final int iterations = 10000;

    public static long addTime(MyNum a, MyNum b) {
        long res = 0;
        for (int i = 0; i < iterations; i++) {
            long before = System.currentTimeMillis();
            a.add(b);
            long after = System.currentTimeMillis();

            res += after - before;
        }
        return res * 1000 / iterations;
    }

    public static long subtractTime(MyNum a, MyNum b) {
        long res = 0;
        for (int i = 0; i < iterations; i++) {
            long before = System.currentTimeMillis();
            a.subtract(b);
            long after = System.currentTimeMillis();

            res += after - before;
        }
        return res * 1000 / iterations;
    }

    public static long multTime(MyNum a, MyNum b) {
        long res = 0;
        for (int i = 0; i < iterations; i++) {
            long before = System.currentTimeMillis();
            a.mult(b);
            long after = System.currentTimeMillis();

            res += after - before;
        }
        return res * 1000 / iterations;
    }

    public static long divTime(MyNum a, MyNum b) {
        long res = 0;
        for (int i = 0; i < iterations; i++) {
            long before = System.currentTimeMillis();
            a.div(b);
            long after = System.currentTimeMillis();

            res += after - before;
        }
        return res * 1000 / iterations;
    }

    public static void main(String[] args) {
        MyNum a = new MyNum(10, "20000");
        MyNum b = new MyNum(10, "11111");

        System.out.println(a.add(b));
        System.out.println(a.subtract(b));
        System.out.println(a.mult(b));
        System.out.println(Arrays.toString(a.div(b)));

        System.out.println(addTime(a, b));
        System.out.println(subtractTime(a, b));
        System.out.println(multTime(a, b));
        System.out.println(divTime(a, b));
    }
}
