public class MyNum {
    int base;
    String digits_str;
    String alphabet = "";

    public MyNum(MyNum other) {
        this.base = other.base;
        this.digits_str = new String(other.digits_str);
        this.alphabet = new String(other.alphabet);
    }

    public MyNum(int base, String digits_str) {
        this.base = base;
        this.digits_str = digits_str;
        this.alphabet = "";
        for (int i = 0; i < base; i++) {
            alphabet += get_char(i);
        }
    }

    public int get_digit(char c) {
        return alphabet.indexOf(c);
    }

    public static char get_char(int i) {
        if (i < 10) {
            return (char) ('0' + i);
        } else {
            return (char) ('a' + (i - 10));
        }
    }

    public MyNum compact() {
        if (digits_str.length() != 1) {
            int i;
            for (i = 0; digits_str.charAt(i) == '0' && i < digits_str.length(); i++) {}
            digits_str = digits_str.substring(i);
        }
        return this;
    }

    public MyNum add(MyNum other) {
        if (base != other.base) throw new RuntimeException("numbers have different bases");
        String res_digits = "";

        int md = 0, n = Math.max(digits_str.length(), other.digits_str.length());
        for (int i = 0; i < n; i++) {
            int sum = get_digit(get_from_end(i)) + other.get_digit(other.get_from_end(i)) + md;
            res_digits = get_char(sum % base) + res_digits;
            md = sum / base;
        }
        if (md != 0) {
            res_digits = get_char(md) + res_digits;
        }

        return new MyNum(base, res_digits);
    }

    public MyNum subtract(MyNum other) {
        if (base != other.base) throw new RuntimeException("numbers have different bases");
        String res_digits = "";

        int md = 0, n = Math.max(digits_str.length(), other.digits_str.length());
        for (int i = 0; i < n; i++) {
            int sum = get_digit(get_from_end(i)) - other.get_digit(other.get_from_end(i)) + md;
            if (sum < 0) {
                sum += base;
                md = -1;
            }
            res_digits = get_char(sum % base) + res_digits;
        }

        return new MyNum(base, res_digits).compact();
    }

    public MyNum mult(MyNum other) {
        if (base != other.base) throw new RuntimeException("numbers have different bases");

        int n = digits_str.length(), t = other.digits_str.length(), b = 0;
        String res_digits = new String(new char[n + t - 1]).replace('\0', '0');
        StringBuilder sb = new StringBuilder(res_digits);

        for (int i = 0; i < n; i++) {
            b = 0;
            for (int j = 0; j < t; j++) {
                int sum = get_digit(sb.charAt(sb.length() - 1 - i - j)) +
                        get_digit(get_from_end(i)) * get_digit(other.get_from_end(j)) +
                        b;
                String part_res = get_num(sum, base);
                sb.setCharAt(n + t - 2 - i - j, (part_res.length() == 1) ? part_res.charAt(0) : part_res.charAt(1));
                b = (part_res.length() == 1) ? 0 : get_digit(part_res.charAt(1));
            }
        }

        if (b != 0) {
            sb.insert(0, get_char(b));
        }

        return new MyNum(base, sb.toString());
    }

    public MyNum[] div(MyNum other) {
        if (base != other.base) throw new RuntimeException("numbers have different bases");

        MyNum r = new MyNum(this);
        MyNum q = zero(base);

        while (r.gte(other)) {
            r = r.subtract(other);
            q = q.add(one(base));
        }

        return new MyNum[] {q, r};
    }

    static String get_num(int n, int base) {
        String res = "";
        int i = 0, x = n, q = x / base;
        res += get_char(x - q * base);
        while (q > 0) {
            i++;
            x = q;
            q = x / base;
            res = res + get_char(x - q * base);
        }
        return res;
    }

    public int get_num() {
        int sum = 0;
        for (int i = digits_str.length() - 1, mult = 1; i >= 0; i--, mult *= base) {
            sum += get_digit(digits_str.charAt(i)) * mult;
        }
        return sum;
    }

    public char get_from_end(int index) {
        int rev_ind = digits_str.length() - 1 - index;
        return rev_ind < 0 ? '0' : digits_str.charAt(rev_ind);
    }

    public boolean gte(MyNum other) {
        return get_num() >= other.get_num();
    }

    public MyNum zero(int base) {
        return new MyNum(base, "0");
    }

    public MyNum one(int base) {
        return new MyNum(base, "1");
    }

    @Override
    public String toString() {
        return "(" + digits_str + " / " + base + ")";
    }
}
